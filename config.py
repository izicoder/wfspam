import json
from enum import Enum, auto
from pathlib import Path

# global script state
IS_WORKING = False


def change_global_state():
    global IS_WORKING
    IS_WORKING = not IS_WORKING
    if IS_WORKING:
        print("working")
    else:
        print("idling")


# milliseconds to seconds
def ms_to_s(milliseconds):
    return 0.001 * milliseconds


# toggle for different modes
class Mode(Enum):
    attack_spam = auto()
    heavy_attack_spam = auto()
    glaive_explosion_spam = auto()


MODE = Mode.glaive_explosion_spam  # defaults to glaive spam


def change_mode(mode):
    global MODE
    MODE = mode
    if mode == Mode.attack_spam:
        print("attack spam mode")
    elif mode == Mode.heavy_attack_spam:
        print("heavy attack spam mode")
    elif mode == Mode.glaive_explosion_spam:
        print("glaive spam mode")


# timings
TIMINGS_FILE = Path("timings.json")

TIMINGS = {
    "hold": 400,
    "before_explosion": 300,
    "after_explosion": 300,
    "attack": 100,
    "heavy_attack": 100,
}

STEP = 10


def save_timings():
    global TIMINGS
    with open(TIMINGS_FILE, "w") as outfile:
        json.dump(TIMINGS, outfile, indent=4)
    print(f"saved timings to {TIMINGS_FILE}")


def load_timings():
    global TIMINGS
    with open(TIMINGS_FILE) as infile:
        TIMINGS = json.load(infile)
    print(f"loaded timings from {TIMINGS_FILE}")


def change_timing(key, minus=False):
    global TIMINGS
    if not minus:
        TIMINGS[key] += STEP
    else:
        TIMINGS[key] = abs(TIMINGS[key] - STEP)
    print(f"{key} timing is {TIMINGS[key]}ms")


# z is normal attack. x is heavy attack. v is alt attack
NORMAL_ATTACK_KEY = "z"
HEAVY_ATTACK_KEY = "x"
ALT_ATTACK_KEY = "v"
