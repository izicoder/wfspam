import time

import keyboard

import config
from config import NORMAL_ATTACK_KEY, HEAVY_ATTACK_KEY
from config import ms_to_s


def glaive_spam():
    keyboard.press(NORMAL_ATTACK_KEY)
    time.sleep(ms_to_s(config.TIMINGS["hold"]))  # normal attack hold timing
    keyboard.release(NORMAL_ATTACK_KEY)
    time.sleep(ms_to_s(config.TIMINGS["before_explosion"]))  # how much to wait for explosion
    keyboard.send(HEAVY_ATTACK_KEY)
    time.sleep(ms_to_s(config.TIMINGS["after_explosion"]))  # how much to wait after explosion


def attack_spam():
    keyboard.send(NORMAL_ATTACK_KEY)
    time.sleep(ms_to_s(config.TIMINGS["attack"]))


def heavy_attack_spam():
    keyboard.send(HEAVY_ATTACK_KEY)
    time.sleep(ms_to_s(config.TIMINGS["heavy_attack"]))
