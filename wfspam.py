import keyboard

import actions
import config

# toggle between running or idling
keyboard.add_hotkey("end", config.change_global_state)

# toggle between different modes
keyboard.add_hotkey("ctrl+alt+1", lambda: config.change_mode(config.Mode.glaive_explosion_spam))
keyboard.add_hotkey("ctrl+alt+2", lambda: config.change_mode(config.Mode.attack_spam))
keyboard.add_hotkey("ctrl+alt+3", lambda: config.change_mode(config.Mode.heavy_attack_spam))

# save timings
keyboard.add_hotkey("ctrl+page up", config.save_timings)
keyboard.add_hotkey("ctrl+page down", config.load_timings)

# hotkeys to change defferent timings
keyboard.add_hotkey("alt+1+=", lambda: config.change_timing("hold"))
keyboard.add_hotkey("alt+1+-", lambda: config.change_timing("hold", minus=True))
keyboard.add_hotkey("alt+2+=", lambda: config.change_timing("before_explosion"))
keyboard.add_hotkey("alt+2+-", lambda: config.change_timing("before_explosion", minus=True))
keyboard.add_hotkey("alt+3+=", lambda: config.change_timing("after_explosion"))
keyboard.add_hotkey("alt+3+-", lambda: config.change_timing("after_explosion", minus=True))
keyboard.add_hotkey("alt+4+=", lambda: config.change_timing("attack"))
keyboard.add_hotkey("alt+4+-", lambda: config.change_timing("attack", minus=True))
keyboard.add_hotkey("alt+5+=", lambda: config.change_timing("heavy_attack"))
keyboard.add_hotkey("alt+5+-", lambda: config.change_timing("heavy_attack", minus=True))

# load timings from a file if it exists
if config.TIMINGS_FILE.exists():
    config.load_timings()

# endless main loop
while True:
    if config.IS_WORKING:
        if config.MODE == config.Mode.glaive_explosion_spam:
            actions.glaive_spam()
        elif config.MODE == config.Mode.attack_spam:
            actions.attack_spam()
        elif config.MODE == config.Mode.heavy_attack_spam:
            actions.heavy_attack_spam()
